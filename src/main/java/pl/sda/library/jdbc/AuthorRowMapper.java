package pl.sda.library.jdbc;

import lombok.extern.slf4j.Slf4j;
import pl.sda.library.common.Author;

import java.sql.ResultSet;
import java.sql.SQLException;

@Slf4j
class AuthorRowMapper {
    static Author getFrom(ResultSet resultSet) {
        try {
            var id = resultSet.getInt("id");
            var firstName = resultSet.getString("first_name");
            var lastName = resultSet.getString("last_name");
            var author = new Author();
            author.setId(id);
            author.setFirstName(firstName);
            author.setLastName(lastName);
            return author;
        } catch (SQLException e) {
            log.error("Something went wrong", e);
            throw new RuntimeException(e);
        }
    }
}
