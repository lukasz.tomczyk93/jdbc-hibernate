package pl.sda.library.jdbc;

import lombok.extern.slf4j.Slf4j;
import pl.sda.library.common.*;

import java.sql.Date;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;

@Slf4j
class LibraryMain {

    private static BooksRepository booksRepository;
    public static void main(String[] args) {

        try (var connection = ConnectionFactory.createMySqlConnection()) {
            booksRepository = new BooksJdbcRepository(connection);
            //TODO: tutaj wywołujemy to co chcemy przetestować.
            testGetBookDetailsById();

        } catch (SQLException e) {
            log.error("Something went wrong", e);
        }
    }

    private static void testGetBookDetailsById() throws SQLException {
        var bookId = 1;
        booksRepository.getBookDetailsById(bookId).ifPresent(bookDetails -> log.info("{}", bookDetails));
    }

    private static void testCreateBook() throws SQLException {
        var author = new Author();
        author.setId(1);

        var category = new Category();
        category.setId(3);

        var newBook = new BookDetails();
        newBook.setTitle("Newly created book");
        newBook.setAuthor(author);
        newBook.setCategory(category);
        newBook.setPublisher("Helion");
        newBook.setReleaseDate(Date.valueOf(LocalDate.of(2022, 5, 29)));
        booksRepository.createBook(newBook);
    }

    private static void testDeleteBook(int bookId) throws SQLException {
        booksRepository.deleteBookById(bookId);
    }

    private static void testGetAllBooks() throws SQLException {
        List<BookBasicInfo> allBooks = booksRepository.getAllBooks();
        allBooks.forEach(LibraryMain::logBookBasicInfo);
    }

    private static void testGetBookById(int requestedBookId) throws SQLException {
        var bookBasicInfoOpt = booksRepository.getBookBasicInfoById(requestedBookId);
        var bookBasicInfo = bookBasicInfoOpt.orElseThrow(
                () -> new IllegalStateException(String.format("Could not found book with id %s", requestedBookId)));
        logBookBasicInfo(bookBasicInfo);
    }

    private static void testUpdateBookTitle(int requestedBookId) throws SQLException {
        var newTitle = "Mistrz czystego kodu. Kodeks postępowania profesjonalnych programistów";
        booksRepository.updateBookTitle(newTitle, requestedBookId);
    }

    private static void logBookBasicInfo(BookBasicInfo bookBasicInfo) {
        log.info("Found book: id: {}, title: {}",
                bookBasicInfo.getId(),
                bookBasicInfo.getTitle());
    }

    private static void testUpdateBook() throws SQLException {
        //przykład do testu
        var author = new Author();
        author.setId(2);

        var category = new Category();
        category.setId(4);

        var bookDetails = new BookDetails();
        bookDetails.setId(1);
        bookDetails.setTitle("Nowy tutuł"); //zmiana z Czysty Agile. Powrót do podstaw
        bookDetails.setCategory(category); //zmiana z 3 na 4
        bookDetails.setAuthor(author); //zmiana z 1 na 2
        bookDetails.setPublisher("Nowe wydawnictwo"); // zmiana z Helion
        bookDetails.setReleaseDate(Date.valueOf(LocalDate.of(2020, 6, 24))); //zmiana dnia z 23 na 24
        booksRepository.updateBook(bookDetails);
    }

    private static void testGetBooksCount() throws SQLException {
        long booksCount = booksRepository.getBooksCount();
        log.info("Number of all books: {}", booksCount);
    }
}