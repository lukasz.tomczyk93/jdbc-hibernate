package pl.sda.library.jdbc;

import lombok.extern.slf4j.Slf4j;
import pl.sda.library.common.Rent;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

@Slf4j
class RentsRowMapper {
    static Rent getFrom(ResultSet resultSet) {
        try {
            var rentId = resultSet.getInt("id");
            var rentDate = resultSet.getDate("rent_date");
            var returnDate = resultSet.getDate("return_date");
            var rent = new Rent();
            rent.setId(rentId);
            rent.setRentDate(rentDate.toLocalDate());
            rent.setReturnDate(Optional.ofNullable(returnDate)
                    .map(Date::toLocalDate)
                    .orElse(null));
            return rent;
        } catch (SQLException e) {
            log.error("Something went wrong", e);
            throw new RuntimeException(e);
        }
    }
}
