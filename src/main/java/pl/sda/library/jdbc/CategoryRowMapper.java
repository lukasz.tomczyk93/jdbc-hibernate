package pl.sda.library.jdbc;

import lombok.extern.slf4j.Slf4j;
import pl.sda.library.common.Category;

import java.sql.ResultSet;
import java.sql.SQLException;

@Slf4j
class CategoryRowMapper {
    static Category getFrom(ResultSet resultSet) {
        try {
            var id = resultSet.getInt("id");
            var name = resultSet.getString("name");
            var description = resultSet.getString("description");
            var category = new Category();
            category.setId(id);
            category.setName(name);
            category.setDescription(description);
            return category;
        } catch (SQLException e) {
            log.error("Something went wrong", e);
            throw new RuntimeException(e);
        }
    }
}
