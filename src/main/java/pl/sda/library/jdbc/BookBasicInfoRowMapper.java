package pl.sda.library.jdbc;

import pl.sda.library.common.BookBasicInfo;

import java.sql.ResultSet;
import java.sql.SQLException;

class BookBasicInfoRowMapper {

    static BookBasicInfo getFrom(ResultSet resultSet) {
        try {
            int bookId = resultSet.getInt("id");
            String bookTitle = resultSet.getString("title");
            return BookBasicInfo.builder()
                    .id(bookId)
                    .title(bookTitle)
                    .build();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}