package pl.sda.library.jdbc;

import com.mysql.cj.jdbc.MysqlDataSource;
import org.h2.jdbcx.JdbcDataSource;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

class ConnectionFactory {

    static Connection createMySqlConnection() throws SQLException {
        return DriverManager.getConnection("jdbc:mysql://localhost:3306/library", "admin", "Password1");
    }

    static Connection createH2Connection() throws SQLException {
        var jdbcDataSource = new JdbcDataSource();
        jdbcDataSource.setURL("jdbc:h2:mem:tes_db");
        jdbcDataSource.setUser("user");
        jdbcDataSource.setPassword("");
        return jdbcDataSource.getConnection();
    }
}