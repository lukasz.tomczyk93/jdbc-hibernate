package pl.sda.library.jdbc;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import pl.sda.library.common.Author;
import pl.sda.library.common.BookDetails;
import pl.sda.library.common.Category;
import pl.sda.library.common.Rent;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

@Slf4j
@RequiredArgsConstructor
class BookDetailsRowMapper {

    private final Connection connection;

    public BookDetails getFrom(ResultSet resultSet) {
        try {
            var bookDetails = new BookDetails();
            var bookId = resultSet.getInt("id");
            var categoryId = resultSet.getInt("category");
            var authorId = resultSet.getInt("author");
            bookDetails.setId(bookId);
            bookDetails.setTitle(resultSet.getString("title"));
            bookDetails.setReleaseDate(resultSet.getDate("release_date"));
            bookDetails.setPublisher(resultSet.getString("publisher"));
            bookDetails.setRents(getRentsForBook(bookId));
            bookDetails.setCategory(getCategory(categoryId));
            bookDetails.setAuthor(getAuthor(authorId));
            return bookDetails;
        } catch (SQLException e) {
            log.error("Something went wrong", e);
            throw new RuntimeException(e);
        }
    }

    private Set<Rent> getRentsForBook(int bookId) throws SQLException {
        var selectRentsForBook = "select r.id, r.rent_date, r.return_date from rents r where r.book = ?";
        var rentsStatement = connection.prepareStatement(selectRentsForBook);
        rentsStatement.setInt(1, bookId);
        var rentsResultSet = rentsStatement.executeQuery();

        Set<Rent> rents = new HashSet<>();
        while (rentsResultSet.next()) {
            rents.add(RentsRowMapper.getFrom(rentsResultSet));
        }
        return rents;
    }

    private Category getCategory(int categoryId) throws SQLException {
        var selectCategoryById = "select c.id, c.name, c.description from categories c where c.id = ?";
        var categoryStatement = connection.prepareStatement(selectCategoryById);
        categoryStatement.setInt(1, categoryId);
        var categoryResultSet = categoryStatement.executeQuery();
        if (categoryResultSet.next()) {
            return CategoryRowMapper.getFrom(categoryResultSet);
        }
        throw new IllegalArgumentException(String.format("Category not found by provided id: %s", categoryId));
    }

    private Author getAuthor(int authorId) throws SQLException {
        var selectAuthorById = "select a.id, a.first_name, a.last_name from authors a where a.id = ?";
        var authorStatement = connection.prepareStatement(selectAuthorById);
        authorStatement.setInt(1, authorId);
        var authorResultSet = authorStatement.executeQuery();

        if (authorResultSet.next()) {
            return AuthorRowMapper.getFrom(authorResultSet);
        }
        throw new IllegalArgumentException(String.format("Author not found by provided id: %s", authorId));
    }
}
