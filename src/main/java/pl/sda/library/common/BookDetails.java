package pl.sda.library.common;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Date;
import java.util.Set;

@Getter
@Entity
@Table(name = "books")
@Setter
public class BookDetails extends BaseEntity {

    @Column(length = 128)
    private String title;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "category")
    private Category category;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "author")
    private Author author;

    @Column(length = 32)
    private String publisher;

    @Column(name = "release_date")
    private Date releaseDate;

    @OneToMany(cascade = CascadeType.REMOVE, mappedBy = "bookDetails")
    private Set<Rent> rents;
}