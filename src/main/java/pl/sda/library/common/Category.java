package pl.sda.library.common;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "categories")
@Getter
@Setter
@ToString
public class Category extends BaseEntity {

    @Column(length = 32)
    private String name;

    @Column(length = 64)
    private String description;
}
