package pl.sda.introduction.jdbc;

import lombok.extern.slf4j.Slf4j;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

import static pl.sda.introduction.jdbc.SQLs.*;

@Slf4j
class MainJdbc {

    public static void main(String[] args) {
        try(var connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/demo", "admin", "Password1");
            var statement = connection.createStatement();
            var preparedStatement = connection.prepareStatement(GET_MOVIE_BY_ID)) {
            log.info("Successfully connected to database");
            String title = "Avengers";
            preparedStatement.setString(1, title);
            var resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                int movieId = resultSet.getInt("id");
                String movieTitle = resultSet.getString("title");
                log.info("Movie id: {}, movie title: {}", movieId, movieTitle);
            }
        } catch (SQLException e) {
            log.error("Something went wrong", e);
        }
    }
}