package pl.sda.introduction.hibernate;

enum CustomerStatus {
    VIP,
    STANDARD,
    INACTIVE
}