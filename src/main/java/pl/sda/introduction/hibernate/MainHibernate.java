package pl.sda.introduction.hibernate;

import lombok.extern.slf4j.Slf4j;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Stream;

@Slf4j
class MainHibernate {

    private static EntityManagerFactory entityManagerFactory;
    private static EntityManager entityManager;

    public static void main(String[] args) {
        entityManagerFactory = Persistence.createEntityManagerFactory("mysql-shop");
        entityManager = entityManagerFactory.createEntityManager();

        initCustomerStatuses();
        addProductWithDetilas();
        updateProductsPriceInMeetCategory();
        addCustomerWithProducts();

        var query = entityManager.createQuery("select pc from ProductCategory pc", ProductCategory.class);
        var allCategories = query.getResultList();

        allCategories.forEach(
                productCategory -> log.info("Products in category '{}': {}",
                        productCategory.getName(), productCategory.getProducts()));

        var queryProductsWithDetails = entityManager.createQuery("select p from Product p", Product.class);
        List<Product> resultList = queryProductsWithDetails.getResultList();
        for (Product product : resultList) {
            log.info("Product with details: {}", product);
        }

        entityManager.close();
        entityManagerFactory.close();
    }

    private static void addCustomerWithProducts() {
        var productsQuery = entityManager.createQuery("select p from Product p", Product.class);
        var products = productsQuery.getResultList();

        var customer = new Customer();
        customer.setProducts(products);
        customer.setCustomerStatus(CustomerStatus.STANDARD);

        entityManager.getTransaction().begin();
        entityManager.persist(customer);
        entityManager.getTransaction().commit();
    }

    private static void updateProductsPriceInMeetCategory() {
        entityManager.getTransaction().begin();

        var selectAllProductsWithMeetCategory = """
                select p from Product p
                join p.productCategory pc
                where pc.name = :category
                """;
        var query = entityManager.createQuery(selectAllProductsWithMeetCategory, Product.class);
        query.setParameter("category", "Meet");
        var productsInMeetCategory = query.getResultStream();
        productsInMeetCategory.forEach(decreasePrice());

        entityManager.getTransaction().commit();
    }

    private static Consumer<Product> decreasePrice() {
        return product -> {
            log.info("{} - old info", product);
            product.decreasePrice(BigDecimal.valueOf(2.00));
            product.setUpdateDatetime(LocalDateTime.now());
            log.info("{} - new info", product);
        };
    }

    private static void filterProducts() {
        var selectProductsWithSpecificPrice = """
                select p from Product p
                where p.price.priceValue > :providedPrice
                or p.productDescription like :description
                """;

        TypedQuery<Product> query = entityManager.createQuery(selectProductsWithSpecificPrice, Product.class);
        query.setParameter("providedPrice", BigDecimal.valueOf(10));
        query.setParameter("description", "%" + "oes" + "%");
        log.info("----------------------- My products: ");
        Stream<Product> filteredProducts = query.getResultStream();
        filteredProducts.forEach(product -> log.info("{}", product));
    }

    private static ProductCategory createCategory(String name) {
        var productCategory = new ProductCategory();
        productCategory.setName(name);
        return productCategory;
    }

    private static void showProductStats() {
        String sql = """
                select new pl.sda.introduction.hibernate.ProductStats(
                    max(p.price),
                    min(p.price),
                    avg(p.price),
                    sum(p.price))
                from Product p
                """;

        var query = entityManager.createQuery(sql, ProductStats.class);
        ProductStats singleResult = query.getSingleResult();

        log.info("Max: {}", singleResult.getMax());
        log.info("Min: {}", singleResult.getMin());
        log.info("Avg: {}", singleResult.getAvg());
        log.info("Sum: {}", singleResult.getSum());
    }


    private static void addProducts() {
        var other = createCategory("Other");
        var meet = createCategory("Meet");
        var vegetables = createCategory("Vegetables");

        var chips = createProduct("Chips", BigDecimal.valueOf(5.99));
        chips.setProductCategory(other);

        var chicken = createProduct("Chicken", BigDecimal.valueOf(15.25));
        chicken.setProductCategory(meet);

        var pizza = createProduct("Pizza", BigDecimal.valueOf(20.00));
        pizza.setProductCategory(other);

        var tomatoes = createProduct("Tomatoes", BigDecimal.valueOf(6.67));
        tomatoes.setProductCategory(vegetables);

        var potatoes = createProduct("Potatoes", BigDecimal.valueOf(6.67));
        potatoes.setProductCategory(vegetables);

        var fish = createProduct("Fish", BigDecimal.valueOf(10.00));
        fish.setProductCategory(meet);

        entityManager.getTransaction().begin();
        entityManager.persist(chicken);
        entityManager.persist(chips);
        entityManager.persist(pizza);
        entityManager.persist(tomatoes);
        entityManager.persist(potatoes);
        entityManager.persist(fish);

        entityManager.getTransaction().commit();
    }

    private static void addProductWithDetilas() {
        var meet = createCategory("Meet");
        var chicken = createProduct("Chicken", BigDecimal.valueOf(15.25));
        chicken.setProductCategory(meet);

        var productDetailsForChicken = new ProductDetails();
        productDetailsForChicken.setCarbonaceous(40);
        productDetailsForChicken.setFat(15);
        productDetailsForChicken.setProtein(45);
        productDetailsForChicken.setExpiryDate(LocalDate.of(2021, 9, 1));
        productDetailsForChicken.setKcal(150);
        productDetailsForChicken.setProduct(chicken);
        chicken.setProductDetails(productDetailsForChicken);

        entityManager.getTransaction().begin();
        entityManager.persist(chicken);
        entityManager.getTransaction().commit();
    }

    private static Product createProduct(String description, BigDecimal priceValue) {
        var product = new Product();
        product.setProductDescription(description);
        var price = new Price();
        price.setPriceValue(priceValue);
        price.setCurrency("PLN");
        product.setPrice(price);
        return product;
    }

    private static void initCustomerStatuses() {
        entityManager.getTransaction().begin();

//        for (var customerStatus : CustomerStatus.values()) {
//            var status = new Status();
//            status.setName(customerStatus.name());
//            entityManager.persist(status);
//        }

        Arrays.stream(CustomerStatus.values())
                .map(toStatus())
                .forEach(entityManager::persist);

        entityManager.getTransaction().commit();
    }

    private static Function<CustomerStatus, Status> toStatus() {
        return customerStatus -> {
            var status = new Status();
            status.setName(customerStatus.name());
            return status;
        };
    }
}